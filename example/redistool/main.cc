#include "all.h"
#include "hiredis.h"

int main()
{
	#ifdef _WIN64
		WSADATA wsaData;
		int32_t iRet = WSAStartup(MAKEWORD(2, 2), &wsaData);
		assert(iRet == 0);
	#else
		signal(SIGPIPE, SIG_IGN);
		signal(SIGHUP, SIG_IGN);
	#endif
	
	struct timeval timeout = { 1, 500000 }; // 1.5 seconds
	
	const char *ip = "127.0.0.1";
	int16_t port = 6379;
	int16_t port1 = 7000;
	
	RedisContextPtr c =  redisConnectWithTimeout(ip, port, timeout);
	if (c == nullptr || c->err)
	{
		if (c)
		{
			printf("Connection error: %s\n", c->errstr);
		}
		else
		{
			printf("Connection error: can't allocate redis context\n");
		}
		exit(1);
	}
	
	RedisContextPtr c1 =  redisConnectWithTimeout(ip, port1, timeout);
	if (c1 == nullptr || c1->err)
	{
		if (c1)
		{
			printf("Connection error: %s\n", c1->errstr);
		}
		else
		{
			printf("Connection error: can't allocate redis context\n");
		}
		exit(1);
	}
	
	return 0l;
}